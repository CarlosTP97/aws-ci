const express = require("express")
const app = express()
const cors = require("cors")

let port = process.env.PORT || 3001

app.use(
    cors({
        origin: "*"
    })
)

app.get("/", (req, res) => {
    res.send(" AWS EC2 CI")
})

app.listen(port, (err) => {
    if (err) return console.log(err)

    console.log("Corriendo en el puerto " + port)
})
